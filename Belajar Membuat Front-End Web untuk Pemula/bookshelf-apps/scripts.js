// Buat eventlistener untuk ubah label tombol submit form bergantung pada status checkbox sudah dibaca/belum selesai dibaca.
document.getElementById("inputBookIsComplete").addEventListener("click", () => {
    if (document.getElementById("inputBookIsComplete").checked === true ) {
      document.getElementById("bookSubmit").innerText = "Masukkan Buku ke rak Sudah selesai dibaca";
    } else {
      document.getElementById("bookSubmit").innerText = "Masukkan Buku ke rak Belum selesai dibaca";
    }
  });

// Cek ada-tidaknya local storage. Kalau tak ada, buat local storage untuk incompleteBooks dan completedBooks keduanya berupa array kosong.
if (typeof (Storage) !== 'undefined') {
    if (localStorage.getItem("incompleteBooks") === null) {
      let incompleteBooks = []
      let completedBooks = []
      localStorage.setItem("incompleteBooks", JSON.stringify(incompleteBooks));
      localStorage.setItem("completedBooks", JSON.stringify(completedBooks));
    }
  }

  // Masukkan data-data di local storage ke variabel incompleteBooks dan completedBooks.
  let incompleteBooks = JSON.parse(localStorage.getItem("incompleteBooks"));
  let completedBooks = JSON.parse(localStorage.getItem("completedBooks"));

  // Buat fungsi untuk menghasilkan id unik suatu buku.
  function generateId() {
    return +new Date();
  }

  // Fungsi untuk membuat object buku baru.
  function generateTodoObject(id, title, author, year, isCompleted) {
  return {
    id,
    title,
    author,
    year,
    isCompleted
  }
}

// Fungsi untuk menulis innerHTML tampilan buku baru.
function makeBook(bookObject) {
  const textTitle = document.createElement('h2');
  textTitle.innerText = bookObject.title;
 
  const textAuthor = document.createElement('p');
  textAuthor.innerText = bookObject.author;

  const textYear = document.createElement('p');
  textYear.innerText = bookObject.year;

  const textSwitchStatus = document.createElement('p');
  textSwitchStatus.setAttribute('onclick',`switchShelf(${bookObject.id});`);
  textSwitchStatus.innerText = "Tukar rak lokasi buku"

  const textDelete = document.createElement('p');
  textDelete.setAttribute('onclick',`deleteBook(${bookObject.id});`);
  textDelete.innerText = "Hapus Buku"
 
  const textContainer = document.createElement('div');
  textContainer.classList.add('inner');
  textContainer.append(textTitle);
  textContainer.append(textAuthor);
  textContainer.append(textYear);
  textContainer.append(textSwitchStatus);
  textContainer.append(textDelete);

  const container = document.createElement('div');
  container.classList.add('item', 'shadow');
  container.append(textContainer);
  container.setAttribute('id', `todo-${bookObject.id}`);
  
  return container;
}

// Fungsi untuk render-ulang daftar buku incomplete ataupun buku completed.
function refreshShelves() {
  const completedTODOList = document.getElementById('completeBookshelfList');
      completedTODOList.innerHTML = '';
      for (const todoItem of completedBooks) {
        const todoElement = makeBook(todoItem);
        completedTODOList.append(todoElement);
      }

      const uncompletedTODOList = document.getElementById('incompleteBookshelfList');
      uncompletedTODOList.innerHTML = '';
      for (const todoItem of incompleteBooks) {
        const todoElement = makeBook(todoItem);
        uncompletedTODOList.append(todoElement);
      }
}

// Jalankan fungsi render daftar buku ketika halaman website dimuat.
refreshShelves()

// Fungsi untuk memperbaharui data local storage sesuai perubahan terbaru terhadap variabel incompleteBooks ataupun completedBooks.
function saveLocalStorage() {
      localStorage.setItem("incompleteBooks", JSON.stringify(incompleteBooks));
      localStorage.setItem("completedBooks", JSON.stringify(completedBooks));
}

// Fungsi untuk mengubah rak suatu buku berdasarkan id buku-nya.
function switchShelf(bookId)   {
    indexToMove = completedBooks.findIndex(x => x.id === bookId);
    if (indexToMove>-1) {
    let newBook = completedBooks[indexToMove]  
    completedBooks.splice(indexToMove, 1)
    incompleteBooks.push(newBook);
    
    refreshShelves();
    saveLocalStorage();
    } else {
    indexToMove = incompleteBooks.findIndex(x => x.id === bookId);
    let newBook = incompleteBooks[indexToMove]  
    incompleteBooks.splice(indexToMove, 1)
    completedBooks.push(newBook);
    }
    refreshShelves();
    saveLocalStorage();
  }

// Fungsi untuk menghapus sebuah buku berdasarkan id buku-nya.
function deleteBook(bookId) {
  let indexToDelete = incompleteBooks.findIndex(x => x.id === bookId);
  if (indexToDelete>-1) {
  incompleteBooks.splice(indexToDelete, 1)
  }

  indexToDelete = completedBooks.findIndex(x => x.id === bookId);
  if (indexToDelete>-1) {
  completedBooks.splice(indexToDelete, 1)
  }

  refreshShelves();
  saveLocalStorage();
}

const RENDER_EVENT = 'render-todo';

// Fungsi untuk membuat buku baru.
const masukkanBuku = document.getElementById('inputBook');
masukkanBuku.addEventListener('submit', function (event) {
  event.preventDefault();
  let inputBookTitle = document.getElementById("inputBookTitle").value;
  let inputBookAuthor = document.getElementById("inputBookAuthor").value;
  let inputBookYear = document.getElementById("inputBookYear").value;
  let inputBookIsComplete = document.getElementById("inputBookIsComplete").checked;
  let newBook = generateTodoObject(generateId(), inputBookTitle, inputBookAuthor, inputBookYear, inputBookIsComplete);

  if(newBook.isCompleted === false ) {
  incompleteBooks.push(newBook);
  refreshShelves();
  saveLocalStorage();
  }

  if(newBook.isCompleted === true ) {
  completedBooks.push(newBook);
  refreshShelves();
  saveLocalStorage();
  }
});